<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('my_url', 'MobileAPI\ProfileController@my_url');
Route::post('register', 'MobileAPI\LoginController@register');
Route::post('login', 'MobileAPI\LoginController@login');
Route::post('forgotpassword', 'MobileAPI\LoginController@forgotpassword');
Route::post('user_details', 'MobileAPI\LoginController@user_details');
Route::post('products_upload', 'MobileAPI\ProductController@products_upload');
Route::post('products_category', 'MobileAPI\ProductController@products_category');
Route::post('products_home_get', 'MobileAPI\ProductController@products_home_get');
Route::post('edit_products_category', 'MobileAPI\ProductController@edit_products_category');
Route::post('edit_myimage', 'MobileAPI\ProfileController@edit_myimage');
Route::post('edit_profile', 'MobileAPI\ProfileController@edit_profile');
Route::post('products', 'MobileAPI\ProfileController@products');
Route::post('favorites', 'MobileAPI\ProfileController@favorites');
Route::post('add_favorite', 'MobileAPI\ProfileController@addFavorite');
Route::post('remove_favorite', 'MobileAPI\ProfileController@removeFavorite');
Route::post('products_filter', 'MobileAPI\ProductController@productsFilter');
