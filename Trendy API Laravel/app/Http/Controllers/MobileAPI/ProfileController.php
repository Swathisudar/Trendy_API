<?php

namespace App\Http\Controllers\MobileAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Product;

class ProfileController extends Controller
{
     public function edit_myimage(Request $request)
	{
		$files = $request->file('image');
		$user_id = $request->user_id;			
							
		if (isset($files))
		{
			$destinationPath = 'profileimage/';
			$filename = $files->getClientOriginalName();
			$savedFileName = date('Ymdhis')."_".$filename;
			$files->move($destinationPath, $savedFileName);
			$timestamp = date("Y-m-d H:i:s");
		    $name='profileimage'.'/'.$savedFileName;
			$values = array(  );
					 $update=User::where('id',$user_id)->update(['image' => $name]);
					 if($update)
								 
						{ 
							  return "Profile Image Updated Successfully";
						}
		} 
			else
		{
				return "Image not uploaded please upload and check it again"; 
		} 	
	}

	public function my_url(Request $request)
	{
		return url('').'/';
	}

	public function edit_profile(Request $request)
	{
		
	}

	/**
     * Get the products record associated with the user.
     * 
     * @param $request
     */
	public function products(Request $request)
	{
		if( $request->user_id )
		{
			$user = User::find($request->user_id);
			if ( count($user) > 0 )
			{
				$products = $user->products;
				if(count($products) > 0)
				{
					$favorites = (count($user->favorites) > 0 ) ? $user->favorites : null;
					if ($favorites) 
					{
						foreach ($products as $product) 
						{
							foreach($favorites as $favorite)
							{
								if ($product->id == $favorite->id)
								{
									$product['favorite'] = 'yes';
								}
							}
						}
					}
					return $products;
				}
				else
				{
					return "no products available";
				}
			}
			else
			{
				return "valid user ID is required";
			}
		}
		else
		{
			return "valid user ID is required";
		}
	}

	/**
     * Get the favorites record associated with the user.
     * 
     * @param $request
     */
	public function favorites(Request $request)
	{
		if( $request->user_id )
		{
			$user = User::find($request->user_id);
			if ( count($user) > 0 )
			{
				$favorites = $user->favorites;
				return (count($favorites) > 0 ) ?  $favorites : "no favorites available";
			}
			else
			{
				return "valid user ID is required";
			}
		}
		else
		{
			return "valid user ID is required";
		}
	}

	/**
     * Add the product to Favorites record for a particular user.
     * 
     * @param $request
     */
	public function addFavorite(Request $request)
	{
		$user_id = $request->user_id;
		$product_id = $request->product_id;

		if( $user_id && !empty($product_id) && User::all()->contains($user_id) && Product::all()->contains($product_id) )
		{
			$user = User::find($user_id);
			if ( count($user) > 0 )
			{
				if ( count($user->favorites()->where('product_id', $product_id)->get()) > 0 )
				{
					return "product already in favorites list";					
				}
				else
				{
					$user->favorites()->attach($product_id);
					return ( count($user->favorites()->where('product_id', $product_id)->get()) > 0 ) ? "product added to favorites" : "product not added to favorites" ;
				}	
			}
		}
		else
		{
			return "valid user ID and product ID are required";
		}
	}

	/**
     * Add the product to Favorites record for a particular user.
     * 
     * @param $request
     */
	public function removeFavorite(Request $request)
	{
		$user_id = $request->user_id;
		$product_id = $request->product_id;

		if( $user_id && !empty($product_id) && User::all()->contains($user_id) && Product::all()->contains($product_id) )
		{
			$user = User::find($user_id);
			if ( count($user) > 0 )
			{
				if ( count($user->favorites()->where('product_id', $product_id)->get()) > 0 )
				{
					$user->favorites()->detach($product_id);
					return ( count($user->favorites()->where('product_id', $product_id)->get()) > 0 ) ? "product not removed from favorites" : "product removed from favorites" ;
				}
				else
				{
					return "product not in favorite list";
				}
			}
		}
		else
		{
			return "valid user ID and product ID are required";
		}
	}

}
