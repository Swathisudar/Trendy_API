<?php

namespace App\Http\Controllers\MobileAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Mail;
use Auth;
use encryptIt;

class LoginController extends Controller
{
                 public function register(Request $request)    
                 {
                 	  $name = $request->name;
                 	  $p1=$request->password;
                 	  $p2=$request->confirmpassword;
					  $password = encrypt($request->password);
					  $confirmpassword = encrypt($request->confirmpassword);
					  $email = $request->email;
					  $phone = $request->phone;
					  $latitude = $request->latitude;
					  $longitude = $request->longitude;

					  if($latitude == '' && $longitude == '')		   
			{
				$latitude ='';
				$longitude = '';
				$address ='';
			}
			else
			{
				$geolocation = $latitude.','.$longitude;
			$request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false'; 
			$file_contents = file_get_contents($request);
			$json_decode = json_decode($file_contents);
			if(isset($json_decode->results[0])) {
				$response = array();
					foreach($json_decode->results[0]->address_components as $addressComponet) 
					{
						if(in_array('political', $addressComponet->types)) {
								$response[] = $addressComponet->long_name; 
						}
    				}

    if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
    if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; } 
    if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
    if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
    if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }

    if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) {
        "<br/>Address:: ".$first;
         "<br/>street:: ".$second;
         "<br/>city:: ".$fourth;
		$address=$first.','.$second.','.$fourth;
    }
    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) {
         "<br/>Address:: ".$first;
         "<br/>street:: ".$second;
         "<br/>city:: ".$third;
	  $address=$first.','.$second.','.$third;
    }
    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null' ) {
         "<br/>street:: ".$first;
         "<br/>city:: ".$second;
		 $address=$first.','.$second;
    }
    else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
         "<br/>city:: ".$first;
		 $address=$first;
      
    }
 
  }
			}

					  $timestamp = date("Y-m-d H:i:s");
			   		  $selectemail = User::select('email')
					  						->where('email',$email)
					  						->first();					  											 
					 if($selectemail) 						 
						{
							
							echo "This email has already taken";
						}						
					 else
					  {
						 if($p2 == $p1)
						 {
						 	$values = array('name' => $name,'password' => $password,'email' => $email,'phone' => $phone,'created_at' => $timestamp,'latitude' => $latitude ,'longitude' => $longitude ,'address' => $address);
							 $insert=User::insert($values);
							 if($insert)						 
								{ 
									  echo " Registered successfully";
								}
						 }
						 else
						 {
						 	echo " Password and Confirm Password Must Be Same";
						 }
						 							  
				      }
                 }
                 public function login(Request $request)
					{
							 $email = $request->email;
				            $password = $request->password; 
					   if($email!=''&& $password!='')
					   {
							  $details = User::select('*','password')
												  ->where('email',$email)
												  ->first();
							   if(count($details) > 0)
							   {
							   	$pass=decrypt($details->password);
								if($pass == $password)
								{
									 return response($details);
								}
								else
								   {
									   echo "Invalid Credentials";
								   }
							   }
							   else
							   {
							   	echo "User does not exist";
							   }
							   
					   }
					   else
					   {
						   echo "Please enter your details";
					   }
							  
					}
					public function forgotpassword(Request $request)
					{
							 $email=$request->email;
							  $selectemail = User::select('email','password','name')
					  						->where('email',$email)
					  						->first();					  							
							 if($selectemail) 						 
								{									
									$password=decrypt($selectemail->password);
				              	    $data = array( 'email' => $selectemail->email,'password' => $password , 'name' => $selectemail->name);
						             Mail::send('email.resetpassword',$data,function($message) use ($email)
						             {
					                   
					                     $message->to([$email])->subject('Forgot Password Letgo');
					                 });
					                echo "Mail has sent successfully";
								}						
							 else
							  {
								echo "This email is not yet registered";
									  
							  }
                      
						}
					public function user_details(Request $request)
					{
						$id=$request->id;
						return User::where('id',$id)
					  						->first();
					}
}
