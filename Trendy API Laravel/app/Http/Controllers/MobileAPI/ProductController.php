<?php

namespace App\Http\Controllers\MobileAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Product;	
use AnthonyMartin\GeoLocation\GeoLocation as GeoLocation;

class ProductController extends Controller
{
     public function products_upload(Request $request)
	{
		$id = $request->id;
		$productname = $request->productname;
		$price = $request->price;
		$files = $request->file('image');
		$categories = $request->categories;
		$latitude = $request->latitude;
		$longitude = $request->longitude;
		$description = $request->description;

		if(!empty($latitude) && !empty($longitude))		   
		{
			$geolocation = $latitude.','.$longitude;
			$request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false';

			if(strpos(get_headers($request)[0], "200 OK"))
			{
				$file_contents = file_get_contents($request);
				$json_decode = json_decode($file_contents);
				
				if(isset($json_decode->results[0])) {
					$response = array();
					foreach($json_decode->results[0]->address_components as $addressComponet) 
					{
						if(in_array('political', $addressComponet->types)) {
							if(in_array('locality', $addressComponet->types)) 
								$response['locality'] = $addressComponet->long_name;
							else
								$response[] = $addressComponet->long_name;
						}						
    				}

				    if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
				    if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; } 
				    if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
				    if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
				    if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }
				    if(isset($response[6])){ $seventh  =  $response[4];  } else { $seventh  = 'null'; }
				    if(isset($response['locality'])){ $locality  =  $response['locality'];  } else { $locality  = false; }			    

				    if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) 
				    {
						$address= $locality ? $first.','.$second.','.$locality.','.$fourth : $first.','.$second.','.$fourth;
				    }
				    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) 
				    {
					    $address=$locality ? $first.','.$second.','.$locality.','.$third : $first.','.$second.','.$third;
				    }
				    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null' ) 
				    {
					    $address=$locality ? $first.','.$locality.','.$second : $first.','.$second;
				    }
				    else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) 
				    {
						$address=$locality ? $locality.','.$first : $first;
				    }	 
				}
				else {
					return "enter valid geo co-ordinates";
				}
			}
			else {
				return "enter valid geo co-ordinates";
			}
		}
		else {
			return "enter valid geo co-ordinates";
		}			
							
		if (isset($files))
		{
			$destinationPath = 'uploads/';
			$filename = $files->getClientOriginalName();
			$savedFileName = date('Ymdhis')."_".$filename;
			$files->move($destinationPath, $savedFileName);
			$timestamp = date("Y-m-d H:i:s");
		    $name = url('uploads').'/'.$savedFileName;
			$values = array('user_id' => $id,
			                'product_name' => $productname,
							'price' => $price ,
							'image' => $name ,
							'category' => $categories ,
							'latitude' => $latitude ,
							'longitude' => $longitude ,
							'address' => $address,
							'created_at' => $timestamp,
							'description' => $description );
			$insert=Product::insert($values);
			if($insert)
			{ 
				return "Products added succesfully.";
			}
		} 
		else
		{
			return "product image is required"; 
		} 	
	}

	public function products_home_get(Request $request)
	{
		$products=Product::where('status','Active')->orderBy('id','desc')->get();
		if(count($products) > 0)
		{
			return $this->addDistance($request->user_id, $products, false, null, null, $request->latitude, $request->longitude);
		}
		else {
			return "no products available";
		}
				
	}
	 public function products_category(Request $request)
	{
		$category = $request->category;
		$products = Product::where('status','Active')->where('category',$category)->get();
		if(count($products) > 0)
		{
			return $this->addDistance($request->user_id, $products, false, null, null, $request->latitude, $request->longitude);
		}
		else
		{
			return "no products available"; 					  
	  						
		}
				
	}
	 public function edit_products_category(Request $request)
	{
		$id = $request->id;
		$user_id= $request->user_id;
		$productname = $request->productname;
		$price = $request->price;
		$files = $request->file('image');
		$categories = $request->categories;
		$latitude = $request->latitude;
		$longitude = $request->longitude;
		$description = $request->description;
		if($latitude == '' && $longitude == '')		   
		{
			$latitude ='';
			$longitude = '';
			$address ='';
		}
		else	
		{
			$geolocation = $latitude.','.$longitude;
			$request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false'; 
			$file_contents = file_get_contents($request);
			$json_decode = json_decode($file_contents);
			if(isset($json_decode->results[0])) {
				$response = array();
				foreach($json_decode->results[0]->address_components as $addressComponet) 
				{
					if(in_array('political', $addressComponet->types)) {
							$response[] = $addressComponet->long_name; 
					}
				}

			    if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
			    if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; } 
			    if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
			    if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
			    if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }

			    if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) {
			        "<br/>Address:: ".$first;
			         "<br/>street:: ".$second;
			         "<br/>city:: ".$fourth;
					$address=$first.','.$second.','.$fourth;
			    }
			    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) {
			         "<br/>Address:: ".$first;
			         "<br/>street:: ".$second;
			         "<br/>city:: ".$third;
				  $address=$first.','.$second.','.$third;
			    }
			    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null' ) {
			         "<br/>street:: ".$first;
			         "<br/>city:: ".$second;
					 $address=$first.','.$second;
			    }
			    else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
			         "<br/>city:: ".$first;
					 $address=$first;
			      
			    }
			}
		}
			
							
		if (isset($files))
		{
			$destinationPath = 'uploads/';
			$filename = $files->getClientOriginalName();
			$savedFileName = date('Ymdhis')."_".$filename;
			$files->move($destinationPath, $savedFileName);
			$timestamp = date("Y-m-d H:i:s");
		    $name=url('uploads').'/'.$savedFileName;
					$update= Product::where('id',$id)->update(['user_id' => $user_id,'product_name' => $productname,'price' => $price ,'image' => $name ,'category' => $categories, 'latitude' => $latitude ,'longitude' => $longitude, 'address' => $address,'updated_at' => $timestamp,'description' => $description ]);
					 if($update)
								 
						{ 
							  return "Products updated succesfully.";
						}
		} 
			else
		{
				return "Image not uploaded please upload and check it again"; 
		} 	
				
	}

	/**
     * Get the products record for the applied filter.
     * 
     * @param $request
     * @return $products
     */
	public function productsFilter(Request $request)
	{
		$user_id = $request->user_id;
		$sold_products = $request->sold_products;
		$category = $request->category;
		$price_from = $request->price_from;
		$price_to = $request->price_to;
		$location = $request->location;
		$distance_in = $request->distance_in;
		$distance_range = $request->distance_range;
		$latitude = $request->latitude;
		$longitude = $request->longitude;

		if ( $user_id && User::all()->contains($user_id) )
		{
			$products= Product::when($category, function($query) use ($category) {
							return $query->where('category', $category);
						})
						->when(($price_from || $price_to), function($query) use ($price_from, $price_to) {
							$price_from = empty($price_from) ? 0 : $price_from;
							$price_to = empty($price_to) ? $query->max('price') :  $price_to;
							return $query->whereBetween('price', [$price_from, $price_to]);
						})->when(!($sold_products === "true"), function($query) {
							return $query->where('status', 'Active');
						})
						->get();

			if ( !empty($location) && empty($latitude) && empty($longitude) )
			{
				$products = $this->addDistance($user_id, $products, true, $distance_range, $location);
			}
			else if ( !empty($location) && !empty($latitude) && !empty($longitude) ) {
				$products = $this->addDistance($user_id, $products, true, $distance_range, $location, $latitude, $longitude);				
			}
			else if ( empty($location) && !empty($latitude) && !empty($longitude) ) {
				$products = $this->addDistance($user_id, $products, true, $distance_range, null, $latitude, $longitude);				
			}
			else if ( !empty($location) ) {
				$products = $this->addDistance($user_id, $products, true, $distance_range, $location);
			}		
			else {
				$products = $this->addDistance($user_id, $products, true, $distance_range);
			}

			return ( count($products) > 0 ) ? $products : "no products available";		
		}
		else
		{
			return "valid user ID is required";
		}
	}

	/**
     * Check the products record for the Favorites.
     * 
     * @param $user_id, $products
     */
	public function checkFavorites($user_id, $products)
	{
		$user = User::find($user_id);
		if ( count($user) > 0 )
		{
			$favorites = (count($user->favorites) > 0 ) ? $user->favorites : null;
			if ($favorites) 
			{
				foreach ($favorites as $favorite) 
				{
					foreach($products as $product)
					{
						if ($product->id == $favorite->id)
						{
							$product['favorite'] = 'yes';
						}
					}
				}
			}
		}
	}

	/**
     * Get the products record filtered by distance.
     * 
     * @param $user_id, $products, $filter, $distance_range, $location, $latitude, $longitude
     * @return $products
     */
	public function addDistance($user_id, $products, $filter = false, $distance_range = null, $location = null, $latitude = null, $longitude = null)
	{
		$product_id = [];
		
		$user = User::where('id', $user_id)->first();
		if ( count($user) > 0 )
		{
			$favorites = (count($user->favorites) > 0 ) ? $user->favorites : null;
			$r = ($user->distance_in == 'km') ? 6371 : (6371 * 0.621371);

			if ( (empty($latitude) && empty($longitude) && empty($location)) || (!empty($location) && empty($distance_range)) )
			{
				$lat = $user->latitude;
				$long = $user->longitude;
				$lat1 = deg2rad($user->latitude);			

			}
			else if ( empty($latitude) && empty($longitude) && !empty($location) && !empty($distance_range) ) {
				$response = GeoLocation::getGeocodeFromGoogle($location);
				if ( isset($response->results[0]) )
				{					
					$lat = $response->results[0]->geometry->location->lat;
					$long = $response->results[0]->geometry->location->lng;
					$lat1 = deg2rad($lat);
				}
				else {
					$response = GeoLocation::getGeocodeFromGoogle($location);
					if ( isset($response->results[0]) )
					{
						$lat = $response->results[0]->geometry->location->lat;
						$long = $response->results[0]->geometry->location->lng;
						$lat1 = deg2rad($lat);
					}
					else {
						return "valid location name is required";
					}
				}
			}
			else {
				$lat = $latitude;
				$long = $longitude;
				$lat1 = deg2rad($lat);
			}
		}
		else {
			if ( empty($latitude) && empty($longitude) && !empty($location) && !empty($distance_range) ) {
				$response = GeoLocation::getGeocodeFromGoogle($location);
				if ( isset($response->results[0]) )
				{
					$lat = $response->results[0]->geometry->location->lat;
					$long = $response->results[0]->geometry->location->lng;
					$lat1 = deg2rad($lat);
				}
				else {
					$response = GeoLocation::getGeocodeFromGoogle($location);
					if ( isset($response->results[0]) )
					{
						$lat = $response->results[0]->geometry->location->lat;
						$long = $response->results[0]->geometry->location->lng;
						$lat1 = deg2rad($lat);
					}
					else {
						return "valid location name is required";
					}
				}
			}
			else {
				$r = 6371;
				$lat = $latitude;
				$long = $longitude;
				$lat1 = deg2rad($lat);
				$favorites = null;
			}			
		}

		foreach($products as $product)
		{

			if ( ($lat != $product->latitude) && ($long != $product->longitude) && !empty($lat) && !empty($long) )
			{
				$lat2 = deg2rad($product->latitude);
				$lat_diff = ($lat > $product->latitude) 
								? deg2rad($lat - $product->latitude)
								: deg2rad($product->latitude - $lat);
				$long_diff = ($long > $product->longitude) 
								? deg2rad($long - $product->longitude)
								: deg2rad($product->longitude - $long);				

				$a = sin($lat_diff/2) * sin($lat_diff/2) + cos($lat1) * cos($lat2) * sin($long_diff/2) * sin($long_diff/2);
				$c = 2 * atan2(sqrt($a), sqrt(1-$a));
				$d = round($r * $c, 1);

				$product['distance'] = $d;

				( empty($distance_range) && $distance_range != "Everywhere" )
					? ( ( !empty($latitude) && !empty($longitude) && !empty($location) ) 
							? ( (stripos($product->address, $location) !== false) ? array_push($product_id, $product->id) : '' ) 
							: array_push($product_id, $product->id) ) 
					: ( ($d <= $distance_range) 
						? ( ( !empty($latitude) && !empty($longitude) && !empty($location) ) 
							? ( (stripos($product->address, $location) !== false) ? array_push($product_id, $product->id) : '' ) 
							: array_push($product_id, $product->id) ) 
						: '' );
			}
			else if ( ($lat == $product->latitude) && ($long == $product->longitude) )
			{
				$product['distance'] = 0;
				$filter ? array_push($product_id, $product->id) : '';
			}

			if ($favorites) 
			{
				foreach ($favorites as $favorite) 
				{
					if ($product->id == $favorite->id)
					{
						$product['favorite'] = 'yes';
					}
				}
			}
		}	

		return empty($product_id) 
					? ( ( empty($distance_range) && $distance_range != "Everywhere" && empty($latitude) && empty($longitude) && !empty($location) ) 
						? [] 
						: ( ( $distance_range == "Everywhere" ) 
							? $products 
							: array_values($products->whereIn('id', $product_id)->toArray()) ) )
					: array_values($products->whereIn('id', $product_id)->toArray());
	}
}
