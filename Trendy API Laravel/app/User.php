<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'distance_in'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the favorites record associated with the user.
     */
    public function favorites()
    {
        return $this->belongsToMany('App\Product');
    }

    /**
     * Get the products record associated with the user.
     */
    public function products()
    {
        return $this->hasMany('App\Product', 'user_id');
    }
}
